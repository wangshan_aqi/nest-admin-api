Nest在代码组织方式上借鉴了`ng`的方式，在底层上默认了`Express`的基础上可配置`Fastify`的这种灵活方式，满足多种选择。天生就以`Typescript`为类型约束，很适合中大型项目的开发。在配以管道、守卫、拦截器、装饰器等等理念使他与其它Node框架拉开了质的距离，所以这次我们采用他做为API的开发框架，整体开发下来的感觉还是比较符合预期，简单说是越开发越喜欢的感觉。

#### 3.1 基本介绍
- 这次的开发在原有的基础上增加了`webpack-hmr.config.js`文件，主要用来应付项目变大之后，热更新的场景。具体见这个配置文件：[https://gitee.com/jsfront/nest-admin-api/webpack-hmr.config.js](https://gitee.com/jsfront/nest-admin-api/blob/master/webpack-hmr.config.js)，然后在main.ts中增加配置：
```
async function bootstrap() {
  // 热更新
  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }
}
```
- 为防止恶意请求，增加了单一IP的单位时间内请求数的限制。
```
// 设置访问频率
app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15分钟
      max: 1000, // 限制15分钟内最多只能访问1000次
    }),
)
```
- 日常的文档开发由 [@nestjs/swagger](https://docs.nestjs.com/openapi/introduction) 这个包提供
  配置生效之后，加入`@ApiModelProperty()`即可在预览的文档中生效。比如：
```
import { ApiModelProperty } from '@nestjs/swagger';
export class CreateCatDto {
  @ApiModelProperty()
  readonly name: string;
  
  @ApiModelProperty()
  readonly breed: string;
}
```
- 由于比较懒，所以 `Entities` 的生成借助了这个工具：[typeorm-model-generator](https://github.com/Kononnable/typeorm-model-generator)

#### 3.2 技术栈
技术 | 说明 | 官网
----|----|----
Nest | 更优雅的node.js 框架 | [https://docs.nestjs.com/](https://docs.nestjs.com/)
Mysql | 数据库服务 | [https://www.mysql.com/cn/](https://www.mysql.com/cn/)
Typeorm | Orm | [https://typeorm.io/](https://typeorm.io/)
@nestjs/jwt | JWT  | [https://github.com/nestjs/jwt](https://github.com/nestjs/jwt)
class-validator | 数据验证  | [https://github.com/typestack/class-validator](https://github.com/typestack/class-validator)

#### 3.3 文件结构
```javascript
├─config // 配置文件
├─dist // 打包文件
├─entities // 生成实体文件
├─public // 静态资源
│  └─uploads // 上传文件
├─src
│  ├─common // 公共文件
│  │  └─logger
│  ├─config // 配置文件
│  ├─interface // TS文件
│  ├─modules // 业务文件
│  │  ├─app
│  │  ├─article
│  │  ├─auth
│  │  ├─category
│  │  ├─common
│  │  ├─file
│  │  ├─menu
│  │  ├─nav
│  │  ├─role
│  │  ├─tags
│  │  └─user
│  └─shared // 核心文件
│      ├─constants
│      ├─core
│      │  ├─decorator
│      │  ├─exception
│      │  ├─filters
│      │  └─interceptors
│      ├─transformer
│      └─utils
└─test
```

#### 3.4 如何在本地运行
根目录下运行 `npm install`，然后运行 `npm run dev`，
```
// 切换环境
nvm install 16.0.0
nvm use 16.0.0

// 安装依赖
npm install

// 启动项目
npm start

// 清除 node_modules
npm run clean

// 全局安装 rimraf 之后方可使用
npm i rimraf -g

// 清除 node_modules 重新安装依赖
// 等同于 npm run clean && npm install
npm run reinstall

```
#### 3.5 如何上线发布
##### 3.5.1 打包
```javascript
$ cd nest-admin-api
$ npm run build
```
然后上传 `nest-admin-api\dist`目录中的文件，然后在服务器安装 `Node_modules`

##### 3.5.2 PM2绑定进程
PM2配置如图所示
![](https://s3.bmp.ovh/imgs/2022/11/25/48e9a38fecdf9aec.jpg)

##### 3.5.3 配置Nginx
```shell
location / {
  proxy_pass  http://127.0.0.1:3000;
}
```
到此我们的Nest总算发布成功。