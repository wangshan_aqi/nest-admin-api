import { Column, Entity } from 'typeorm'
import { CommonEntity } from '@/modules/common/entities/common.entities'

@Entity('app_info')
export class AppInfoEntity extends CommonEntity {
  @Column('varchar', { name: 'name', comment: '中文名称', length: 250 })
  name: string

  @Column('varchar', { name: 'host', comment: '域名', length: 250 })
  host: string

  @Column('varchar', { name: 'domain', comment: '域名', length: 250 })
  domain: string

  @Column('varchar', { name: 'title', comment: '英文名称', length: 255 })
  title: string

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '状态',
    unsigned: true,
  })
  status: number | null

  @Column('varchar', {
    name: 'remark',
    nullable: true,
    comment: '备注',
    length: 255,
  })
  remark: string | null

  @Column('varchar', {
    name: 'description',
    nullable: true,
    comment: '描述',
    length: 255,
  })
  description: string | null

  @Column('varchar', {
    name: 'order',
    nullable: true,
    comment: '排序',
    length: 255,
  })
  order: string | null

  @Column('int', {
    name: 'city_id',
    nullable: true,
    comment: '城市id',
    unsigned: true,
  })
  cityId: number | null
}
