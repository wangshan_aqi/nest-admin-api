import { Module } from '@nestjs/common'
import { AppService } from './app.service'
import { AppController } from './app.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AppInfoEntity } from './entities/app.entity'

@Module({
  imports: [TypeOrmModule.forFeature([AppInfoEntity])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppInfoModule {}
