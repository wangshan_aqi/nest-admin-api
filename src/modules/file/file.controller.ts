import { Controller, Get, Post, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { FileService } from './file.service'

@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  upload(@UploadedFile('file') file) {
    file.path = file.path.replace(/\\/g, '/').replace('public', 'static')
    return file
  }

  @Get()
  findAll() {
    return this.fileService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileService.findOne(+id)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.fileService.remove(+id)
  }
}
