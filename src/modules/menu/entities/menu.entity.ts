import { Column, Entity, ManyToMany } from 'typeorm'
import { CommonEntity } from '@/modules/common/entities/common.entities'
import { RoleEntity } from '@/modules/role/entities/role.entity'

@Entity('auth_modules')
export class MenuEntity extends CommonEntity {
  @Column('varchar', { name: 'code', comment: 'code', length: 100 })
  code: string

  @Column('varchar', { name: 'title', comment: '模块名称', length: 50 })
  title: string

  @Column('int', {
    name: 'parent_id',
    comment: '父模块编号',
    unsigned: true,
    default: () => "'0'",
  })
  parentId: number

  @Column('varchar', {
    name: 'order',
    nullable: true,
    comment: '排序',
    length: 200,
  })
  order: string | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '文章状态',
    width: 1,
  })
  status: number

  @Column('varchar', { name: 'component', comment: '组件路径', length: 100 })
  component: string

  @Column('varchar', { name: 'path', comment: '模块路径', length: 200 })
  path: string

  @Column('tinyint', { name: 'blank', comment: '是否外链', unsigned: true })
  blank: number

  @Column('tinyint', { name: 'hidden', comment: '是否隐藏', unsigned: true })
  hidden: number

  @Column('varchar', {
    name: 'href',
    nullable: true,
    comment: '链接',
    length: 100,
  })
  href: string | null

  @Column('varchar', { name: 'icon_name', nullable: true, length: 50 })
  iconName: string | null

  @Column('varchar', {
    name: 'target',
    nullable: true,
    comment: '链接方式',
    length: 100,
  })
  target: string | null

  @Column('int', { name: 'city_id', comment: '城市id' })
  cityId: number

  @ManyToMany((type) => RoleEntity, (role) => role.menus)
  roles: RoleEntity[]
}
