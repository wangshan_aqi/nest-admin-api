import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { RoleService } from './role.service'
import { RoleModelsService } from './roleModels.service'
import { CreateRoleDto } from './dto/create-role.dto'
import { UpdateRoleDto } from './dto/update-role.dto'
import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'

@Controller('role')
export class RoleController {
  constructor(private readonly roleService: RoleService, private readonly roleModelsService: RoleModelsService) {}

  @Post()
  create(@Body() createRoleDto: CreateRoleDto) {
    return this.roleService.create(createRoleDto)
  }

  @Get()
  async findAll() {
    const rows = await this.roleService.findAll()
    return { rows, page: { total: rows.length } }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.roleService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRoleDto: UpdateRoleDto) {
    return this.roleService.update(+id, updateRoleDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.roleService.remove(id)
  }

  @Post('createRoleModels')
  createRoleModels(@Body() createRoleDto: CreateRoleDto) {
    return this.roleModelsService.create(createRoleDto)
  }

  @Post('updateRoleModels')
  updateRoleModels(@Param('id') id: string, @Body() updateRoleDto: UpdateRoleDto) {
    return this.roleModelsService.update(+id, updateRoleDto)
  }
}
