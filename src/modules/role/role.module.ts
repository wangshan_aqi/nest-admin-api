import { Module } from '@nestjs/common'
import { RoleService } from './role.service'
import { RoleController } from './role.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { RoleEntity } from './entities/role.entity'
import { RoleModelsEntity } from './entities/roleModels.entity'
import { RoleModelsService } from './roleModels.service'

@Module({
  imports: [TypeOrmModule.forFeature([RoleEntity, RoleModelsEntity])],
  controllers: [RoleController],
  exports: [RoleService],
  providers: [RoleService, RoleModelsService],
})
export class RoleModule {}
