import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreateRoleDto } from './dto/create-role.dto'
import { UpdateRoleDto } from './dto/update-role.dto'
import { RoleModelsEntity } from './entities/roleModels.entity'

@Injectable()
export class RoleModelsService {
  constructor(
    @InjectRepository(RoleModelsEntity)
    private readonly roleModelsRepository: Repository<RoleModelsEntity>,
  ) {}

  async create(dto: CreateRoleDto) {
    const result = await this.roleModelsRepository.create(dto)
    await this.roleModelsRepository.save(result)

    return result
  }

  // 详情
  findOne(id) {
    return this.roleModelsRepository.findOne({ where: { id } })
  }

  // 修改
  async update(id, dto: UpdateRoleDto) {
    const old = await this.findOne(id)
    const vo = await this.roleModelsRepository.merge(old, dto)

    return this.roleModelsRepository.save(vo)
  }
}
