import { Column, Entity } from 'typeorm'
import { CommonEntity } from '@/modules/common/entities/common.entities'

@Entity('auth_role_modules')
export class RoleModelsEntity extends CommonEntity {
  @Column('int', { name: 'role_id', nullable: true, unsigned: true })
  roleId: number | null

  @Column('int', { name: 'module_id', nullable: true, unsigned: true })
  moduleId: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('int', { name: 'city_id', nullable: true, comment: '城市id' })
  cityId: number | null
}
