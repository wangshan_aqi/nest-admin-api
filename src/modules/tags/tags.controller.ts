import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { DeleteParams } from '@/shared/core/decorator/DeleteParams.decorator'
import { TagsService } from './tags.service'
import { CreateTagDto } from './dto/create-tag.dto'
import { UpdateTagDto } from './dto/update-tag.dto'
import { IParamsResult, ListParams } from '@/shared/core/decorator/ListParams.decorator'

@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  @Post()
  create(@Body() createTagDto: CreateTagDto) {
    return this.tagsService.create(createTagDto)
  }

  @Get()
  async findAll(@ListParams({ where: ['type'] }) params: IParamsResult) {
    const rows = await this.tagsService.findAll(params)
    return { rows }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.tagsService.findOne(+id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTagDto: UpdateTagDto) {
    return this.tagsService.update(+id, updateTagDto)
  }

  @Delete(':id')
  remove(@DeleteParams() id: string) {
    return this.tagsService.remove(id)
  }
}
