import { ArticleEntity } from '@/modules/article/entities/article.entity'
import { CommonEntity } from '@/modules/common/entities/common.entities'
import { ApiProperty } from '@nestjs/swagger'
import { Column, Entity, ManyToMany } from 'typeorm'

@Entity('tags')
export class TagsEntity extends CommonEntity {
  @Column('int', { name: 'city_id', nullable: true, unsigned: true })
  cityId: number | null

  @Column('int', { name: 'type', nullable: true, unsigned: true })
  type: number | null

  @Column('varchar', { name: 'tag_name', length: 255 })
  tagName: string

  @Column('varchar', { name: 'tag_url', length: 100 })
  tagUrl: string | null

  @Column('varchar', { name: 'thumbnail', nullable: true, length: 1023 })
  thumbnail: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @ApiProperty()
  @ManyToMany(() => ArticleEntity, (article) => article.tags)
  articles: ArticleEntity[]
}
