import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsMobilePhone, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator'

export class CreateUserDto {
  @ApiProperty({ description: '用户名', example: 'tony' })
  @IsString({ message: '用户名为字符串类型' })
  @IsNotEmpty({ message: '用户名不能为空' })
  @MinLength(5, { message: '账号至少5个字符' })
  @MaxLength(20, { message: '账号最多20个字符' })
  readonly username: string

  @ApiProperty({ description: '昵称', example: '不才' })
  readonly nick_name: string

  @ApiProperty({ description: '城市id' })
  city_id: string

  @ApiProperty({ description: '密码' })
  @IsString({ message: 'password 类型错误，正确类型 string' })
  @IsNotEmpty({ message: 'password 不能为空' })
  password: string

  @ApiProperty({ description: '手机号', required: false })
  @IsString({ message: 'phoneNum 类型错误，正确类型 string' })
  @IsMobilePhone('zh-CN', { strictMode: false }, { message: '请输入正确的手机号' })
  @IsOptional()
  readonly phone?: string

  @ApiProperty({ description: '邮箱', required: false })
  @IsString({ message: 'email 类型错误，正确类型 string' })
  @IsEmail()
  @IsOptional()
  readonly email?: string

  // @ApiProperty({ description: '确认密码' })
  // @IsString({ message: ' confirmPassword 类型错误，正确类型 string' })
  // readonly confirmPassword: string
}
