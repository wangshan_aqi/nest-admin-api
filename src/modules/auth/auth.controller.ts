import { IParamsResult, ListParams } from '@/shared/core/decorator/ListParams.decorator'
import { Controller, Post, Body, UseGuards, Get } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { UserService } from '../user/user.service'
import { AuthService } from './auth.service'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private readonly userService: UserService) {}
  /**
   * 用户登录
   * @param user
   */
  @Post('login')
  async login(@Body() user) {
    return await this.authService.login(user)
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('users')
  async findAll(@ListParams({ where: ['username'], order: { id: 'DESC' } }) params: IParamsResult) {
    return await this.userService.findAll(params)
  }

  @Get('list')
  async findList(@ListParams({ where: ['username'], order: { id: 'DESC' } }) params: IParamsResult) {
    return await this.userService.findAll(params)
  }
}
