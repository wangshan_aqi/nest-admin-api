import { ArticleEntity } from '@/modules/article/entities/article.entity'
import { CommonEntity } from '@/modules/common/entities/common.entities'
import { NavEntity } from '@/modules/nav/entities/nav.entity'
import { Column, Entity, ManyToMany, OneToMany } from 'typeorm'

@Entity('article_category')
export class CategoryEntity extends CommonEntity {
  @Column('int', { name: 'model_id', comment: '模块 id' })
  modelId: number

  @Column('smallint', {
    name: 'parent_id',
    comment: '父类 id',
    default: () => "'0'",
  })
  parentId: number

  @Column('int', { name: 'level', comment: '菜单等级' })
  level: number

  @Column('varchar', { name: 'small_img', comment: '缩略图', length: 100 })
  smallImg: string

  @Column('varchar', { name: 'title', comment: '标题', length: 255 })
  title: string

  @Column('varchar', {
    name: 'description',
    nullable: true,
    comment: '描述',
    length: 100,
  })
  description: string | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '状态',
    width: 1,
  })
  status: boolean | null

  @Column('tinyint', {
    name: 'order',
    nullable: true,
    comment: '排序',
    width: 1,
  })
  order: boolean | null

  @Column('int', { name: 'city_id', nullable: true, unsigned: true })
  cityId: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @OneToMany((type) => ArticleEntity, (article: any) => article.category)
  article: ArticleEntity[]

  @ManyToMany((type) => NavEntity, (nav) => nav.categories)
  navs: NavEntity[]
}
