import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { isString } from 'lodash'

// 自定义条件
export interface IParamsConfig {
  where?: string[]
}
export interface IParamsResult {
  id: string | string[]
}

export const DeleteParams = createParamDecorator((customConfig: IParamsConfig, ctx: ExecutionContext): string => {
  const request = ctx.switchToHttp().getRequest()
  const { params } = request
  const { id: ids } = params
  let id

  // 处理批量删除
  if (isString(ids)) {
    id = ids.includes(',') ? ids.split(',') : ids
  }

  return id
})
