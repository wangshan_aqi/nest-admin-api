import * as dayjs from 'dayjs'
import { ValueTransformer } from 'typeorm'

// 时间格式化，防止出现2022-0101T09:12:23.102Z
export class TimestampTransformer implements ValueTransformer {
  constructor(private readonly formatstr: string = 'YYYY-MM-DD HH:mm:ss') {}

  to(value: any) {
    return value
  }

  from(value: any) {
    return dayjs(value).format(this.formatstr)
  }
}
