import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('article_tags', { schema: 'vue-admin' })
export class ArticleTags extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number

  @Column('int', { name: 'article_id' })
  articleId: number

  @Column('int', { name: 'tags_id' })
  tagsId: number

  @Column('varchar', { name: 'tag_name', nullable: true, length: 255 })
  tagName: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('int', {
    name: 'city_id',
    nullable: true,
    comment: '城市id',
    unsigned: true,
  })
  cityId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
