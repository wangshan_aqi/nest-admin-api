import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('article_info', { schema: 'vue-admin' })
export class ArticleInfo extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '编号',
    unsigned: true,
  })
  id: number

  @Column('int', {
    name: 'city_id',
    nullable: true,
    comment: '城市 id',
    unsigned: true,
  })
  cityId: number | null

  @Column('int', { name: 'category_id', comment: '分类 id', unsigned: true })
  categoryId: number

  @Column('int', { name: 'user_id', comment: '用户 id', unsigned: true })
  userId: number

  @Column('varchar', { name: 'title', comment: '标题', length: 255 })
  title: string

  @Column('text', { name: 'content', comment: '内容' })
  content: string

  @Column('text', { name: 'md', nullable: true, comment: 'markdown' })
  md: string | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '文章状态',
    width: 1,
  })
  status: boolean | null

  @Column('varchar', {
    name: 'keywords',
    nullable: true,
    comment: '关键字',
    length: 100,
  })
  keywords: string | null

  @Column('varchar', {
    name: 'cover_url',
    nullable: true,
    comment: '封面图',
    length: 100,
  })
  coverUrl: string | null

  @Column('int', {
    name: 'views_count',
    nullable: true,
    comment: '浏览量',
    unsigned: true,
    default: () => "'0'",
  })
  viewsCount: number | null

  @Column('int', {
    name: 'like_count',
    nullable: true,
    comment: '点赞数',
    unsigned: true,
    default: () => "'0'",
  })
  likeCount: number | null

  @Column('int', {
    name: 'comment_count',
    nullable: true,
    comment: '评论数',
    unsigned: true,
    default: () => "'0'",
  })
  commentCount: number | null

  @Column('varchar', {
    name: 'summary',
    nullable: true,
    comment: '摘要',
    length: 1000,
  })
  summary: string | null

  @Column('smallint', { name: 'is_top', nullable: true, comment: '是否置顶' })
  isTop: number | null

  @Column('varchar', {
    name: 'author',
    nullable: true,
    comment: '作者',
    length: 30,
  })
  author: string | null

  @Column('smallint', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
  })
  order: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    comment: '修改时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
