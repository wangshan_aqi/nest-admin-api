import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('nav', { schema: 'vue-admin' })
export class Nav extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('varchar', {
    name: 'name',
    nullable: true,
    comment: '名称',
    length: 100,
  })
  name: string | null

  @Column('varchar', {
    name: 'title',
    nullable: true,
    comment: '中文名称',
    length: 100,
  })
  title: string | null

  @Column('int', { name: 'parent_id', nullable: true })
  parentId: number | null

  @Column('int', {
    name: 'category_id',
    nullable: true,
    comment: '分类id',
    unsigned: true,
  })
  categoryId: number | null

  @Column('int', {
    name: 'order',
    nullable: true,
    comment: '排序',
    unsigned: true,
  })
  order: number | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '状态',
    width: 1,
  })
  status: boolean | null

  @Column('varchar', {
    name: 'small_img',
    nullable: true,
    comment: '封面图片',
    length: 100,
  })
  smallImg: string | null

  @Column('int', { name: 'city_id', nullable: true })
  cityId: number | null

  @Column('int', { name: 'app_id', nullable: true })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
