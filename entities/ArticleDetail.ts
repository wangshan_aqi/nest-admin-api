import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('article_detail', { schema: 'vue-admin' })
export class ArticleDetail extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '序号',
    unsigned: true,
  })
  id: number

  @Column('int', {
    name: 'article_id',
    comment: '文章 id',
    unsigned: true,
    default: () => "'0'",
  })
  articleId: number

  @Column('text', {
    name: 'content_md',
    nullable: true,
    comment: '文章markdown',
  })
  contentMd: string | null

  @Column('text', {
    name: 'content_html',
    nullable: true,
    comment: '文章html内容',
  })
  contentHtml: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('int', {
    name: 'city_id',
    nullable: true,
    comment: '城市id',
    unsigned: true,
  })
  cityId: number | null

  @Column('datetime', { name: 'create_time', comment: '创建时间' })
  createTime: Date

  @Column('datetime', { name: 'update_time', comment: '修改时间' })
  updateTime: Date
}
