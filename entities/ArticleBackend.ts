import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('article_backend', { schema: 'vue-admin' })
export class ArticleBackend extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('int', {
    name: 'article_id',
    nullable: true,
    comment: '文章id',
    unsigned: true,
  })
  articleId: number | null

  @Column('int', {
    name: 'backend_id',
    nullable: true,
    comment: '后台id',
    unsigned: true,
  })
  backendId: number | null

  @Column('varchar', {
    name: 'backend_name',
    nullable: true,
    comment: '后台名称',
    length: 100,
  })
  backendName: string | null

  @Column('varchar', {
    name: 'backend_url',
    nullable: true,
    comment: '后台url',
    length: 100,
  })
  backendUrl: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    comment: '更新时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
