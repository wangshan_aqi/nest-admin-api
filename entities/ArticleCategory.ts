import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('article_category', { schema: 'vue-admin' })
export class ArticleCategory extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('int', {
    name: 'model_id',
    comment: '模块 id',
    unsigned: true,
    default: () => "'0'",
  })
  modelId: number

  @Column('smallint', {
    name: 'parent_id',
    comment: '父类 id',
    default: () => "'0'",
  })
  parentId: number

  @Column('int', { name: 'level', nullable: true, comment: '菜单等级' })
  level: number | null

  @Column('varchar', {
    name: 'small_img',
    nullable: true,
    comment: '缩略图',
    length: 100,
  })
  smallImg: string | null

  @Column('varchar', { name: 'title', comment: '标题', length: 255 })
  title: string

  @Column('varchar', {
    name: 'description',
    nullable: true,
    comment: '描述',
    length: 100,
  })
  description: string | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '状态',
    width: 1,
  })
  status: boolean | null

  @Column('tinyint', {
    name: 'order',
    nullable: true,
    comment: '排序',
    width: 1,
  })
  order: boolean | null

  @Column('int', { name: 'city_id', nullable: true, unsigned: true })
  cityId: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date

  @Column('datetime', {
    name: 'update_time',
    comment: '修改时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date
}
