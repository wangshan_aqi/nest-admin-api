import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('auth_users', { schema: 'vue-admin' })
export class AuthUsers extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: 'id',
    unsigned: true,
  })
  id: number

  @Column('varchar', { name: 'customer_level', comment: '等级', length: 255 })
  customerLevel: string

  @Column('varchar', {
    name: 'username',
    nullable: true,
    comment: '登录名',
    length: 255,
  })
  username: string | null

  @Column('tinyint', {
    name: 'status',
    nullable: true,
    comment: '用户状态',
    width: 1,
  })
  status: boolean | null

  @Column('varchar', {
    name: 'avatar',
    nullable: true,
    comment: '用户头像',
    length: 100,
  })
  avatar: string | null

  @Column('varchar', {
    name: 'nick_name',
    nullable: true,
    comment: '昵称',
    length: 255,
  })
  nickName: string | null

  @Column('varchar', {
    name: 'password',
    nullable: true,
    comment: '密码',
    length: 255,
  })
  password: string | null

  @Column('varchar', {
    name: 'salt',
    nullable: true,
    comment: '加密盐',
    length: 100,
  })
  salt: string | null

  @Column('tinyint', {
    name: 'sex',
    nullable: true,
    comment: '性别',
    unsigned: true,
  })
  sex: number | null

  @Column('date', { name: 'birthday', nullable: true, comment: '生日' })
  birthday: string | null

  @Column('varchar', {
    name: 'email',
    nullable: true,
    comment: '邮箱',
    length: 50,
  })
  email: string | null

  @Column('varchar', {
    name: 'remark',
    nullable: true,
    comment: '备注',
    length: 255,
  })
  remark: string | null

  @Column('varchar', {
    name: 'phone',
    nullable: true,
    comment: '手机号',
    length: 255,
  })
  phone: string | null

  @Column('int', { name: 'city_id', comment: '城市id', unsigned: true })
  cityId: number

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('datetime', {
    name: 'create_time',
    nullable: true,
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date | null

  @Column('datetime', {
    name: 'update_time',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date | null
}
