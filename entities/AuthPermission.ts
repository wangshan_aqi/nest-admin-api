import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'

@Index('permissionValueUnique', ['permissionValue'], { unique: true })
@Entity('auth_permission', { schema: 'vue-admin' })
export class AuthPermission extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '权限id',
    unsigned: true,
  })
  id: number

  @Column('varchar', {
    name: 'permission_value',
    nullable: true,
    unique: true,
    comment: '权限值',
    length: 50,
  })
  permissionValue: string | null

  @Column('varchar', {
    name: 'permission_module',
    nullable: true,
    comment: '权限模块',
    length: 50,
  })
  permissionModule: string | null

  @Column('varchar', {
    name: 'permission_name',
    nullable: true,
    comment: '权限名称',
    length: 50,
  })
  permissionName: string | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('int', { name: 'city_id', nullable: true, comment: '城市id' })
  cityId: number | null

  @Column('datetime', { name: 'create_time', comment: '创建时间' })
  createTime: Date

  @Column('datetime', { name: 'update_time', comment: '修改时间' })
  updateTime: Date
}
