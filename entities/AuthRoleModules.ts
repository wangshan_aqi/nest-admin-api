import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm'

@Index('rolemod', ['roleId', 'moduleId'], { unique: true })
@Index('FK_RoleModules_Modules_ModuleId', ['moduleId'], {})
@Index('FK_RoleModules_Roles_RoleId', ['roleId'], {})
@Entity('auth_role_modules', { schema: 'vue-admin' })
export class AuthRoleModules extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
  id: number

  @Column('int', { name: 'role_id', nullable: true, unsigned: true })
  roleId: number | null

  @Column('int', { name: 'module_id', nullable: true, unsigned: true })
  moduleId: number | null

  @Column('int', {
    name: 'app_id',
    nullable: true,
    comment: '应用id',
    unsigned: true,
  })
  appId: number | null

  @Column('int', { name: 'city_id', nullable: true, comment: '城市id' })
  cityId: number | null

  @Column('datetime', {
    name: 'create_time',
    comment: '创建时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createTime: Date

  @Column('datetime', {
    name: 'update_time',
    comment: '修改时间',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateTime: Date
}
